import React from 'react';
import Table from 'react-table';
import striptags from 'striptags';
import axios from 'axios';
import moment from 'moment';
import lf from 'localforage';
import { Tooltip, Button, Modal, Icon, Popconfirm, Select, message } from 'antd';

const kibana_host = "http://localhost:5601/app/kibana#";

class PersonalList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      showShareFormModal: false,
      sharedPeoples: [],
      data: {},
      userList: [],
    };

    this.table = React.createRef();
  }

  componentDidMount() {
    this.loadUsers();
  }

  componentDidUpdate(prevProps) {
    const { user } = this.props;

    // User object got updated
    if (prevProps.user != user) {
      this.fetchData({ page: 0 }, user);
    }
  }

  loadUsers = () => {
    axios.get('http://localhost:9000/user/ldap')
      .then(({ data }) => {
        this.setState({ userList: data.data });
      })
      .catch(({ response }) => {
        message.error(response.data.message);
      });
  }

  fetchData = ({ pageSize, page }, user) => {
    this.setState({ loading: true });

    axios.get("/proxy/kibana", {params: {
      path: '/api/saved_objects/_find',
      type: 'dashboard',
      search_fields: 'optionsJSON',
      search: `"uid ${user.uid}"~0`,
      page: page + 1,
    }})
      .then(({ data }) => {
        this.setState({ loading: false, data });
      })
      .catch(({ response }) => {
        this.setState({ loading: false });
        message.error(response.data.message);
      });
  }

  render() {
    const { user, onSetDefault, defaultDashboard } = this.props;
    const { loading, data, userList, sharedPeoples, showShareFormModal } = this.state;

    return (
      <Table
        ref={this.table}
        manual
        filterable
        pageSize={data.per_page}
        defaultPageSize={10}
        data={data.saved_objects}
        pages={data.total}
        loading={loading}
        onFetchData={param => this.fetchData(param, user)}
        className="f6"
        columns={[
          {Header: 'Name', width: 300, Cell: ({ original }) => original.id == defaultDashboard ? (
            <div>
              <b>{original.attributes.title}</b>
              <Icon type="star" theme="filled" className="ml2" />
            </div>
          ) : original.attributes.title},
          {Header: 'Description', Cell: ({ original }) => striptags(original.attributes.description)},
          {Header: 'Last Update', Cell: ({ original }) => (
            <div className="tc">
              {moment(original.updated_at).fromNow()}
            </div>
          )},
          {
            Header: 'Action',
            Cell: ({ original }) => (
              <div className="tc">
                <Tooltip title="Make Default">
                  <Button type="default" icon="star" className="mr2" onClick={() => onSetDefault(original)} />
                </Tooltip>

                <Tooltip title="View">
                  <a href={`${kibana_host}/dashboard/${original.id}`} className="mr2" target="_blank">
                    <Button type="default" icon="eye" />
                  </a>
                </Tooltip>

                <Tooltip title="Edit">
                  <a href={`${kibana_host}/dashboard/${original.id}?_a=(viewMode:edit)`} className="mr2" target="_blank">
                    <Button type="default" icon="edit" />
                  </a>
                </Tooltip>

                <Tooltip title="Share">
                  <Button
                    type="primary"
                    icon="share-alt"
                    className="mr2"
                    onClick={() => {
                      const optionsJSON = JSON.parse(original.attributes.optionsJSON);
                      this.setState({showShareFormModal: true, sharedPeoples: optionsJSON.shared || []});
                      // todo: update optionsJSON.shared data from react table
                    }} />
                </Tooltip>

                <Modal
                  title="Share Dashboard"
                  visible={showShareFormModal}
                  onCancel={() => this.setState({ showShareFormModal: false, sharedPeoples: [] })}
                  onOk={() => {
                    this.setState({ loading: true, showShareFormModal: false });

                    const optionsJSON = JSON.parse(original.attributes.optionsJSON);
                    optionsJSON.shared = sharedPeoples;

                    const formData = {
                      data: {
                        attributes: {optionsJSON: JSON.stringify(optionsJSON)},
                        references: original.references,
                      },
                    };

                    const config = {params: {path: `/api/saved_objects/dashboard/${original.id}`}};

                    axios.put('/proxy/kibana', formData, config)
                      .then(() => {
                        message.success('Dashboard shared successfully!');
                        this.setState({ loading: false });
                      })
                      .catch(({ response }) => {
                        this.setState({ loading: false });
                        message.error(response.data.message);
                      });
                  }}
                >
                  <Select
                    mode="multiple"
                    placeholder="Please select"
                    className="w-100"
                    value={sharedPeoples}
                    onChange={value => this.setState({ sharedPeoples: value })}
                  >
                    {userList
                      .filter(({ uid }) => uid !== user.uid && uid !== 'admin')
                      .map(({ uid, name, email }) => (
                        <Select.Option key={uid}>
                          <div>{name}</div>
                          <div className="f7 gray">{email}</div>
                        </Select.Option>
                      ))}
                  </Select>
                </Modal>

                <Popconfirm
                  placement="left"
                  title="Are you sure ?"
                  onConfirm={() => {
                    this.setState({ loading: true });

                    axios.delete('/proxy/kibana', {params: {path: `/api/saved_objects/dashboard/${original.id}`}})
                      .then(() => {
                        this.setState({
                          loading: false,
                          data: {...data, saved_objects: data.saved_objects.filter(item => item.id !== original.id)},
                        });
                      })
                      .catch((err) => {
                        this.setState({ loading: false });
                        message.error(err.message);
                      });
                  }}
                >
                  <Tooltip title="Delete">
                    <Button type="danger" icon="delete" />
                  </Tooltip>
                </Popconfirm>
              </div>
            ),
          },
        ]}
      />
    );
  }
}

export default PersonalList;
