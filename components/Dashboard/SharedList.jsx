import React from 'react';
import Table from 'react-table';
import striptags from 'striptags';
import axios from 'axios';
import moment from 'moment';
import lf from 'localforage';
import { Tooltip, Button, Modal, Icon, Popconfirm, Select, message } from 'antd';

const kibana_host = "http://localhost:5601/app/kibana#";

class SharedList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: true,
      data: {},
    };

    this.table = React.createRef();
  }

  componentDidUpdate(prevProps) {
    const { user } = this.props;

    // User object got updated
    if (prevProps.user != user) {
      this.fetchData({ page: 0 }, user);
    }
  }

  fetchData = ({ pageSize, page }, user) => {
    this.setState({ loading: true });

    axios.get("/proxy/kibana", {params: {
      path: '/api/saved_objects/_find',
      type: 'dashboard',
      search_fields: 'optionsJSON',
      search: `(-("uid ${user.uid}"~0)+("shared ${user.uid}"~100))`,
      page: page + 1,
    }})
      .then(({ data }) => {
        this.setState({ loading: false, data });
      })
      .catch(({ response }) => {
        this.setState({ loading: false });
        message.error(response.data.message);
      });
  }

  render() {
    const { user, onSetDefault, defaultDashboard } = this.props;
    const { loading, data } = this.state;

    return (
      <Table
        ref={this.table}
        manual
        filterable
        pageSize={data.per_page}
        defaultPageSize={10}
        data={data.saved_objects}
        pages={data.total}
        loading={loading}
        onFetchData={param => this.fetchData(param, user)}
        className="f6"
        columns={[
          {Header: 'Name', width: 300, Cell: ({ original }) => original.id == defaultDashboard ? (
            <div>
              <b>{original.attributes.title}</b>
              <Icon type="star" theme="filled" className="ml2" />
            </div>
          ) : original.attributes.title},
          {Header: 'Description', Cell: ({ original }) => striptags(original.attributes.description)},
          {Header: 'Last Update', Cell: ({ original }) => (
            <div className="tc">
              {moment(original.updated_at).fromNow()}
            </div>
          )},
          {
            Header: 'Action',
            Cell: ({ original }) => (
              <div className="tc">
                <Tooltip title="Make Default">
                  <Button type="default" icon="star" className="mr2" onClick={() => onSetDefault(original)} />
                </Tooltip>

                <Tooltip title="View">
                  <a href={`${kibana_host}/dashboard/${original.id}`} className="mr2" target="_blank">
                    <Button type="default" icon="eye" />
                  </a>
                </Tooltip>
              </div>
            ),
          },
        ]}
      />
    );
  }
}

export default SharedList;
