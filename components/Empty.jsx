import React from 'react';
import { Icon } from 'antd';

const EmptyDisplay = (
  <div className="tc f2 pa5 silver">
    <Icon type="inbox" className="f2" /> Empty
  </div>
);

class Empty extends React.Component {
  render() {
    const { isEmpty = false, display = EmptyDisplay, children } = this.props;
    return isEmpty ? display : children;
  }
}

export default Empty;
