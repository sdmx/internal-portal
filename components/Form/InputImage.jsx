import React from 'react';
import { Upload, Icon, message } from 'antd';

class InputImage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      previewUrl: null,
    };
  }

  getBase64(img, callback) {
    console.log(img)
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result));
    reader.readAsDataURL(img);
  }

  validateImage() {
    return false; // prevent auto upload
  }

  setPreview(file) {
    this.setState({ loading: true })

    if (file.originFileObj) {
      this.getBase64(file.originFileObj, imageUrl => this.setState({
        previewUrl: imageUrl,
        loading: false,
      }));
    }
    else {
      this.setState({
        previewUrl: "error",
        loading: false,
      })
    }
  }

  render() {
    const { onChange, ...attrs } = this.props;
    const { previewUrl } = this.state;

    return (
      <Upload
        listType="picture-card"
        beforeUpload={this.validateImage}
        showUploadList={false}
        onChange={info => {
          this.setPreview(info.file);

          if (!!onChange) {
            onChange(info.file)
          }
        }}
        {...attrs}
      >
        {previewUrl ? (
          <img src={previewUrl} alt={previewUrl} />
        ) : (
          <div>
            <Icon type={this.state.loading ? 'loading' : 'plus'} />
            <div className="ant-upload-text">Upload</div>
          </div>
        )}
      </Upload>
    );
  }
}

export default InputImage;
