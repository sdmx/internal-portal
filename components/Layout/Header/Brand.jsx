import React from 'react';

import Link from '~/components/Link';

export default class Header extends React.Component {
  render() {
    const { sidebarWidth = 230 } = this.props;

    return (
      <div className="flex">
        <Link
          href="/"
          className="tc bg-white ph2 h-100 b--light-gray"
          style={{ width: `${sidebarWidth}px`, float: 'left' }}
        >
          <img src="/static/img/logo-bi.gif" alt="Logo" style={{ width: 'auto' }} />
        </Link>

        <div
          style={{
            border: '32px solid transparent',
            borderTopColor: '#fff',
            borderLeftColor: '#fff',
          }}
        />
      </div>
    );
  }
}
