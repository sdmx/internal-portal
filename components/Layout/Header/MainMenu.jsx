import React from 'react';
import { Menu } from 'antd';

import Link from '~/components/Link';

export default class Header extends React.Component {
  render() {
    const { menu = [] } = this.props;

    return (
      <Menu mode="horizontal" className="bg-white dib" style={{ lineHeight: '64px' }}>
        {menu && menu.map(({ url, html = false, label }) => (
          <Menu.Item key={url}>
            {html
              ? (<a href={url} className="gray">{label}</a>)
              : (<Link href={url} className="gray">{label}</Link>)
            }
          </Menu.Item>
        ))}
      </Menu>
    );
  }
}
