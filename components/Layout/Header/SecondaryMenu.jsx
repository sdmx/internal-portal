import React from 'react';
import { Menu, Icon, Dropdown, Badge } from 'antd';

import Link from '~/components/Link';
import NotificationPanel from '~/components/Layout/Header/NotificationPanel';

export default class SecondaryMenu extends React.Component {
  render() {
    const { menu = [] } = this.props;

    return (
      <Menu
        mode="horizontal"
        style={{ lineHeight: '60px', marginTop: '-2px' }}
        className="bg-transparent white bw0"
      >
        {/*<Menu.Item
          key="notification"
          className="hover-bg-primary tc"
          style={{ border: 'none' }}
        >
          <Dropdown
            className="pa3"
            placement="bottomCenter"
            overlay={(<NotificationPanel />)}
            // trigger={['click']}
          >
            <Badge count={0}>
              <Icon
                type="bell"
                theme="filled"
                className="white"
                style={{ margin: 0, fontSize: '24px', paddingTop: '3px' }}
              />
            </Badge>
          </Dropdown>
        </Menu.Item>*/}

        {menu && menu.map(({ id, subs, url, attr, label, icon }, i) => {
          const key = id || i;

          if (!subs) {
            return (
              <Menu.Item key={key}>
                <Link href={url} {...attr}>{label}</Link>
              </Menu.Item>
            );
          }

          return (
            <Menu.Item key={key} className="hover-bg-primary">
              <Dropdown
                placement="bottomRight"
                overlay={(
                  <Menu>
                    {subs && subs.map((item) => {
                      const itemIcon = item.icon && (
                        <Icon
                          type={item.icon}
                          style={{ fontSize: '16px' }}
                          className="ph1"
                        />
                      );

                      return (
                        <Menu.Item key={item.url}>
                          {item.html ? (
                            <a href={item.url} className="white f6" {...item.attr}>
                              {itemIcon} {item.label}
                            </a>
                          ) : (
                            <Link href={item.url} className="white f6" {...item.attr}>
                              {itemIcon} {item.label}
                            </Link>
                          )}
                        </Menu.Item>
                      );
                    })}
                  </Menu>
                )}
              >
                <div className="white pointer ph3 f6">
                  {icon && (<Icon type={icon} style={{ fontSize: '18px' }} className="ph1" />)}
                  {label}
                </div>
              </Dropdown>
            </Menu.Item>
          );
        })}
      </Menu>
    );
  }
}
