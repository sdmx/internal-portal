import React from 'react';
import { Layout } from 'antd';

import { Auth as ApiAuth } from '~/lib/api';
import app from '~/config/app.json';

import Brand from '~/components/Layout/Header/Brand';
import MainMenu from '~/components/Layout/Header/MainMenu';
import SecondaryMenu from '~/components/Layout/Header/SecondaryMenu';

export default class Header extends React.Component {
  render() {
    const { user, styleAttr } = this.props;

    return (
      <Layout.Header className="header pa0 z-1 bg-primary flex justify-between">
        <Brand {...styleAttr} />

        <MainMenu />

        <SecondaryMenu
          menu={[
            {
              id: 'user',
              url: '/',
              label: user.name,
              icon: 'user',
              subs: [
                {
                  url: app.ADMIN_URL,
                  label: 'Administration',
                  icon: 'setting',
                  html: true,
                  attr: {
                    target: '_blank',
                  },
                },
                {
                  url: '/auth/logout',
                  label: 'Logout',
                  icon: 'logout',
                  attr: {
                    onClick: () => ApiAuth.logout(false),
                  },
                },
              ],
            },
          ]}
        />
      </Layout.Header>
    );
  }
}
