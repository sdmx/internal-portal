import React from 'react';

import Layout from '~/components/Layout';
import app from '~/config/app.json';

class AdminLayout extends React.Component {
  render() {
    const leftMenu = [
      { url: '/', icon: 'home', label: 'Home' },
      { url: app.DASHBOARD_URL, icon: 'dashboard', label: 'Dashboard' },
      { url: app.RSTUDIO_URL, icon: 'user', label: 'RStudio' },
      { url: app.ADMIN_URL, icon: 'setting', label: 'Administration' },
    ];

    return (
      <Layout left={leftMenu}>
        {this.props.children}
      </Layout>
    );
  }
}

export default AdminLayout;
