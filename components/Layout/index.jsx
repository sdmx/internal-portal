import React from 'react';
import classnames from 'classnames';
import { Layout as AntLayout, Menu, Icon, Dropdown, message, Tooltip } from 'antd';

import 'react-quill/dist/quill.snow.css';
import 'antd/dist/antd.min.css';
import 'react-table/react-table.css';
import 'nprogress/nprogress.css';

import Link from '~/components/Link';
import Loading from '~/components/Loading';
import Header from '~/components/Layout/Header';
import Auth from '~/lib/auth';
import { Auth as ApiAuth } from '~/lib/api';
import app from '~/config/app.json';

import '~/public/static/css/tachyons.min.css';
import '~/public/static/css/override.css';
import '~/public/static/css/custom.css';

const { Content, Sider } = AntLayout;

const sidebarWidth = 230;

class Layout extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {},
      loading: true,
    };

    Auth.user().then(user => this.setState({ user }));

    ApiAuth.init()
      .then(() => this.setState({ loading: false }))
      .catch((err) => {
        message.error(err.message);
        this.setState({ loading: false });
      });
  }

  render() {
    const { user, loading, collapsedLeft, collapsedRight } = this.state;
    const { children, noPadding = false } = this.props;

    const left = [
      { url: '/', icon: 'home', label: 'Home' },
      { url: app.RSTUDIO_URL, icon: 'project', label: 'RStudio', noRouter: true },
    ];

    return (
      <AntLayout className="bg-white" style={{ minHeight: '100vh' }}>
        <Header user={user} styleAttr={{ sidebarWidth }} />

        <Loading isFinish={!loading}>
          <AntLayout>
            {left && (
              <Sider collapsible trigger={null} width={collapsedLeft ? 71 : 230} className="bg-primary pt3">
                <Content>
                  <Menu mode="inline" selectedKeys={['1']} style={{ height: '100%', borderRight: 0 }} className="bg-primary">
                    {left && left.map(item => (
                      <Menu.Item key={item.label}>
                        <Tooltip title={collapsedLeft ? item.label : null} placement="right">
                          <Link
                            href={item.url}
                            className="default"
                            noRouter={item.noRouter}
                            {...{target: item.noRouter ? '_blank' : '_self'}}
                          >
                            {item.icon && (
                              <Icon
                                className="white"
                                type={item.icon}
                                style={collapsedLeft
                                  ? { fontSize: '30px' }
                                  : { fontSize: '15px' }
                                }
                              />
                            )}

                            <span className="white">
                              {!collapsedLeft && item.label}
                            </span>
                          </Link>
                        </Tooltip>
                      </Menu.Item>
                    ))}
                  </Menu>
                </Content>
              </Sider>
            )}

            {/* Content */}
            <AntLayout
              id="page-content"
              className={classnames({'shadow-in pt3 pa4 pb5': !noPadding})}
              style={{background: '#fafbfd'}}
            >
              <div style={{ minHeight: 280 }}>
                {children}
              </div>
            </AntLayout>
          </AntLayout>
        </Loading>
      </AntLayout>
    );
  }
}

export default Layout;
