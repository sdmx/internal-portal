import React from 'react';
import PropType from 'prop-types';
import NextLink from 'next/link';
import { Button } from 'antd';

const Link = ({
  href,
  children,
  button,
  prefetch,
  noRouter,
  ...props
}) => {
  /*const content = (
    <div>
      {button
        ? (<Button href={href} {...props}>{children}</Button>)
        : (<a href={href} className="black-70" {...props}>{ children }</a>)}
    </div>
  );

  return <div>{noRouter ? content : <NextLink prefetch={prefetch} href={href}> {content} </NextLink>}</div>*/
  const buttonEle = <Button href={href} {...props}>{children}</Button>
  const anchorEle = <a href={href} className="black-70" {...props}>{ children }</a>

  return (
    noRouter ? (
      button ? buttonEle : anchorEle
    ) : (
      <NextLink prefetch={prefetch} href={href}>
        {button ? buttonEle : anchorEle}
      </NextLink>
    )
  );
}

Link.propTypes = {
  href: PropType.string.isRequired,
  children: PropType.node,
  button: PropType.bool,
  prefetch: PropType.bool,
  noRouter: PropType.bool,
};

Link.defaultProps = {
  button: false,
  prefetch: true,
  noRouter: false,
  children: '',
};

export default Link;
