import React from 'react';
import { Icon } from 'antd';

const LoadingDisplay = (
  <div className="w-100 h-100 tc" style={{ marginTop: '20%' }}>
    <Icon type="loading" className="f1" />
  </div>
);

class Loading extends React.Component {
  render() {
    const { isFinish = true, display = LoadingDisplay, children } = this.props;

    return isFinish ? children : display;
  }
}

export default Loading;
