import React from 'react';

const PageHeader = ({ title, action }) => (
  <div className="flex justify-between bb b--moon-gray pb2 mb2">
    <div className="f2"> {title} </div>
    <div className="tr pt2"> {action} </div>
  </div>
);

export default PageHeader;
