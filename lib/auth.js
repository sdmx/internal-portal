import axios from 'axios';
import _ from 'lodash';
import store from 'store';
import expirePlugin from 'store/plugins/expire';

import redirect from './redirect';

store.addPlugin(expirePlugin);

const fetchAsync = (req, success, error) => {
  const funcSuccess = _.isFunction(success);
  const funcError = _.isFunction(error);

  if (funcSuccess || funcError) {
    if (funcSuccess) req.then(success);
    if (funcError) req.catch(error);

    return req;
  }

  return false;
};

const Auth = {
  async check(success, error) {
    const req = axios.get('/auth/check');

    // Asynchronous
    const fetch = fetchAsync(req, success, error);
    if (fetch) return fetch;

    // Synchronous
    try {
      await req;
      return true;
    }
    catch(err) {
      return false;
    }
  },

  user() {
    return new Promise((resolve, reject) => {
      const cache = store.get('user');

      if (cache) {
        resolve(cache);
      }
      else {
        axios.get('/auth/user')
          .then(({ data }) => {
            store.set('user', data, new Date().getTime() + (1000 * 60 * 10));
            resolve(data);
          })
          .catch(e => reject(e));
      }
    });
  },

  logout(doRedirect = true) {
    store.remove('user');

    if (doRedirect) {
      window.location = '/auth/logout';
    }
  },

  token() {
    return new Promise((resolve, reject) => {
      axios.get('/auth/token')
        .then(({ data }) => resolve(data))
        .catch(err => reject(err));
    });
  },

  hasRole(roles) {
    try {
      const user = user();

      if (_.isArray(roles)) {
        return _.intersection(user.roles, roles).length === roles.length;
      }

      return _.includes(user.roles, roles);
    }
    catch (err) {
      return false;
    }
  },

  redirectIfAuthenticated(res) {
    Auth.check(() => redirect('/', res));
  },

  redirectIfNotAuthenticated(res) {
    Auth.check(null, () => redirect('/', res));
  },
};

export default Auth;
