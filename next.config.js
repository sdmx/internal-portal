const withCSS = require('@zeit/next-css');
const Dotenv = require('dotenv-webpack');

// fix: prevents error when .css files are required by node
if (typeof require !== 'undefined') {
  require.extensions['.css'] = () => {};
}

module.exports = withCSS({
  webpack(config, { dev }) {
    config.plugins.push(new Dotenv());
    config.module.rules.push({
      test: /\.(eot|woff|woff2|ttf|svg|png|jpg|gif)$/,
      use: {
        loader: 'url-loader',
        options: {
          limit: 100000,
          name: '[name].[ext]',
        },
      },
    });
    // config.module.rules.push({
    //   test: /\.svg$/,
    //   use: [{
    //     loader: 'babel-loader',
    //   }, {
    //     loader: 'react-svg-loader',
    //   }],
    // });
    // config.module.rules.push({
    //   test: /\.svg$/,
    //   use: ['@svgr/webpack'],
    // });

    return config;
  },
});
