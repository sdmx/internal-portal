import React from 'react';
import Document, { Head, Main, NextScript } from 'next/document';

export default class MainDocument extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx);
    return { ...initialProps };
  }

  render() {
    return (
      <html lang={this.props.locale}>
        <Head>
          <meta charSet="utf-8" />
          <meta httpEquiv="x-ua-compatible" content="ie=edge" />
          <title>{process.env.APP_NAME}</title>
          <meta name="description" content="SOENDA" />
          <meta name="viewport" content="width=device-width, initial-scale=1" />

          {/*<link rel="manifest" href="/static/site.webmanifest" />*/}
          <link rel="shortcut icon" href="/static/favicon.ico" />
          {/*<link rel="stylesheet" type="text/css" href="/_next/static/style.css" />*/}
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </html>
    );
  }
}
