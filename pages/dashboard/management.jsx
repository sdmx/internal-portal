import React from 'react';
import axios from 'axios';
import localforage from 'localforage';
import Router from 'next/router';
import { message, Icon, Button, Tabs } from 'antd';
import lf from 'localforage';

import Link from '~/components/Link';
import Layout from '~/components/Layout';
import PersonalList from '~/components/Dashboard/PersonalList';
import SharedList from '~/components/Dashboard/SharedList';
import Auth from '~/lib/auth';
import page from '~/page';
import app from '~/config/app.json';

const kibana_host = "http://localhost:5601/app/kibana#";

class Management extends React.Component {
  static getInitialProps({ res }) {
    if (res) Auth.redirectIfNotAuthenticated(res);
    return {};
  }

  constructor(props) {
    super(props);
    this.state = {
      btnCreateLoading: false,
      dashboard: {},
      user: {},
      storage: {},
    };
  }

  componentDidMount() {
    Auth.user().then(user => {
      this.setState({ user });

      lf.getItem(`s-${user.uid}`, (err, storage) => {
        if (storage !== null) {
          this.setState({ storage });
        }
      });
    });
  }

  setDefaultDashboard = ({ id }) => {
    const { storage, user } = this.state;
    const values = {...storage, dashboard: id};

    lf.setItem(`s-${user.uid}`, values, () => {
      this.setState({ storage: values });
      message.success('Dashboard set as default successfully!');
    });
  }

  render() {
    const { btnCreateLoading, user, storage } = this.state;

    return (
      <Layout>
        <div className="pb2">
          <Button
            loading={btnCreateLoading}
            type="primary"
            icon="plus"
            onClick={() => {
              this.setState({ btnCreateLoading: true });

              const formData = {
                data: {
                  attributes: {
                    title: "New Dashboard",
                    hits: 0,
                    description: "",
                    panelsJSON: "[]",
                    optionsJSON: JSON.stringify({useMargins: true, hidePanelTitles: false, uid: user.uid}),
                    version: 1,
                    timeRestore: false,
                    kibanaSavedObjectMeta: {
                      searchSourceJSON: JSON.stringify({query: { query:"", language:"kuery" }, filter: []})
                    },
                  },
                },
              };

              axios.post('/proxy/kibana', formData, {params: {path: '/api/saved_objects/dashboard'}})
                .then(({ data }) => {
                  // window.location.href = `${kibana_host}/dashboard/${data.id}?_a=(viewMode:edit)`;
                  const url = `${kibana_host}/dashboard/${data.id}?_a=(viewMode:edit)`;
                  const win = window.open(url, '_blank');
                  win.focus();
                })
                .catch(err => {
                  this.setState({ btnCreateLoading: false });
                  message.error(err.message);
                });
            }}
          >
            Create New Dashboard
          </Button>
        </div>

        <Tabs defaultActiveKey="personal">
          <Tabs.TabPane tab="Personal" key="personal">
            <PersonalList user={user} defaultDashboard={storage.dashboard} onSetDefault={this.setDefaultDashboard} />
          </Tabs.TabPane>
          <Tabs.TabPane tab="Shared" key="shared">
            <SharedList user={user} defaultDashboard={storage.dashboard} onSetDefault={this.setDefaultDashboard} />
          </Tabs.TabPane>
        </Tabs>
      </Layout>
    );
  }
}

export default page(Management);
