import React from 'react';
import lf from 'localforage';
import axios from 'axios';
import { Icon } from 'antd';

import Link from '~/components/Link';
import Layout from '~/components/Layout';
import Auth from '~/lib/auth';
import page from '~/page';
import app from '~/config/app.json';

const id = "";
const kibana_endpoint = "http://localhost:5601/app/kibana#";

class Homepage extends React.Component {
  static getInitialProps({ res }) {
    if (res) Auth.redirectIfNotAuthenticated(res);
    return {};
  }

  constructor(props) {
    super(props);
    this.state = {
      storage: {},
      dashboardMeta: {},
    };
  }

  componentDidMount() {
    Auth.user().then(user => {
      this.setState({ user });

      lf.getItem(`s-${user.uid}`, (err, storage) => {
        if (storage !== null) {
          this.setState({ storage });

          axios.get('/proxy/kibana', {params: {path: `/api/saved_objects/dashboard/${storage.dashboard}`}})
            .then(({ data }) => {
              this.setState({ dashboardMeta: data });
            })
            .catch(({ response }) => {
              console.log(response);
            });
        }
      });
    });
  }

  render() {
    const { storage, dashboardMeta } = this.state;

    return (
      <Layout noPadding>
        <div className="cf pa2 bg-light-gray">
          <div className="fl w-80">
            {!!dashboardMeta.attributes && (
              <span className="b f4">{dashboardMeta.attributes.title}</span>
            )}
          </div>
          <div className="fr w-20 tr">
            <Link button href="/dashboard/management" className="bg-primary white b">
              <Icon type="setting" theme="filled" />
              <span>Management</span>
            </Link>
          </div>
        </div>

        <iframe
          src={`http://localhost:5601/app/kibana#/dashboard/${storage.dashboard}?embed=true&_g=()`}
          style={{ height: '520px', width: '100%' }}
          frameborder="0"
        />
      </Layout>
    );
  }
}

export default page(Homepage);
