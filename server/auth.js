const jwt = require("jsonwebtoken");
const axios = require("axios");
const url = require('url');

const REDIRECT_URI = "/";

module.exports = (app) => {

  const validate = (token) => {
    return new Promise((resolve, reject) => {
      jwt.verify(token, process.env.SECRET_KEY, function(err, decoded) {
        if (err)
          reject(err);
        else
          resolve(decoded);
      });
    });
  }

  app.get('/auth/login', function(req, res) {
    res.redirect(url.format({
      pathname: `${process.env.AUTH_ENDPOINT}/authenticate`,
      query: {
        client_id: process.env.APP_CODE,
        redirect_url: url.format({
          protocol: req.protocol,
          host: req.get('host'),
          pathname: "/auth/callback",
        }),
      },
    }));

    res.end();
  });

  app.get('/auth/logout', function(req, res) {
    validate(req.session.token)
      .then((jws) => {
        req.session.destroy(() => {
          res.redirect(url.format({
            pathname: `${process.env.AUTH_ENDPOINT}/logout`,
            query: {
              client_id: process.env.APP_CODE,
              redirect_url: url.format({
                protocol: req.protocol,
                host: req.get('host'),
                pathname: REDIRECT_URI,
              })
            },
          }));
          res.end();
        });
      })
      .catch((err) => {
        res.status(401);
        res.end();
      });
  });

  app.get('/auth/callback', function(req, res) {
    const { token } = req.query;

    validate(token)
      .then((jws) => {
        req.session.token = token;
        res.redirect(REDIRECT_URI);
        res.end();
      })
      .catch((err) => {
        res.redirect(REDIRECT_URI);
        res.end();
      });
  });

  app.get("/auth/user", function(req, res) {
    const { token } = req.session;

    validate(token)
      .then((jws) => {
        axios.get(`${process.env.AUTH_ENDPOINT}/auth/info/${token}`)
          .then(({ data }) => res.send(data))
          .catch(err => {
            res.status(401);
            res.send(err);
            res.end();
          });
      })
      .catch((err) => {
        res.status(401);
        res.end();
      });
  });

  app.get('/auth/check', function(req, res) {
    validate(req.session.token)
      .then((jws) => res.end())
      .catch((err) => {
        res.status(401);
        res.end();
      });
  });

  app.get('/auth/token', function(req, res) {
    const { token } = req.session;

    validate(token)
      .then((jws) => {
        res.send(token);
        res.end();
      })
      .catch((err) => {
        res.status(401);
        res.end();
      });
  });

  app.use(function(req, res, next) {
    const segments = req.url.split(/[\/\\\\]/).filter(segment => segment.trim().length > 0);

    if (segments.length > 0) {
      switch (segments[0]) {
        case 'api':
          return next();

        case 'analytic':
          res.redirect(url.format({
            pathname: `${process.env.AUTH_ENDPOINT}/authenticate`,
            query: {
              client_id: process.env.APP_CODE,
              redirect_url: url.format({
                pathname: `${process.env.ANALYTIC_ENDPOINT}`,
              }),
            },
          }));

          res.end();
      }
    }

    validate(req.session.token)
      .then((jws) => next())
      .catch((err) => {
        res.redirect("/auth/login");
        res.end();
      });
  });
}
