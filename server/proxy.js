const FormData = require('form-data');
const axios = require("axios");
const url = require('url');
const qs = require('querystring');

const REDIRECT_URI = "/";

module.exports = (app) => {
  app.all('/proxy/kibana', function(req, res) {
    const kibana_endpoint = 'http://localhost:5601';
    const { data = {} } = req.body;
    const { path = '/', ...params } = req.query;

    const config = {
      params,
      headers: {
        'kbn-xsrf': 'reporting',
        'Content-Type': 'application/x-www-form-urlencoded',
      }
    };

    // handle request
    let axiosReq;
    const formData = {};

    for (const key in data) {
      formData[key] = typeof data[key] === 'object' ? JSON.stringify(data[key]) : data[key];
    }

    switch (req.method) {
      case 'GET':
        axiosReq = axios.get(kibana_endpoint + path, config);
        break;

      case 'DELETE':
        axiosReq = axios.delete(kibana_endpoint + path, config);
        break;

      case 'PUT':
        axiosReq = axios.put(kibana_endpoint + path, qs.stringify(formData), config)
        break;

      case 'POST':
        axiosReq = axios.post(kibana_endpoint + path, qs.stringify(formData), config)
        break;
    }

    axiosReq.then(({ data }) => {
        res.send(data);
        res.end();
      })
      .catch(({ response }) => {
        res.status(response.status);
        res.send(response.data);
        res.end();
      });
  });
}
